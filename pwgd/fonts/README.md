# Fonts 

See `visionsunite/src/global/_fonts.scss`

TODO consider citing *only* woff especially if there's a risk some browser might load more than one, or one less preferred than woff.

Agaric is including these custom fonts in the same way as we are for MassDesignGroup.org

It's working well on a site that cares about performance (aided by Cloudflare)
but we will repeat Todd Linkner's note:

> You may want to use Google Fontloader or another method 
> to improve performance and mitigate FOUT. 

## Font sources

### Libre Caslon

https://www.fontsquirrel.com/fonts/libre-caslon

[SIL Open Font License.txt](SIL Open Font License.txt)
